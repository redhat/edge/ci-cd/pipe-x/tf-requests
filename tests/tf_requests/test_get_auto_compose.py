import json
from unittest.mock import MagicMock, patch

import pytest
import requests_mock

from tf_requests.get_auto_compose import (
    ImageInfoError,
    calculate_auto_compose,
    get_auto_compose_id,
    get_image_info,
    validate_ami,
    validate_compose,
)

# Sample URLs and data
WEBSERVER_RELEASES = "http://example.com/releases"
RELEASE_NAME = "ER0"
IMAGE_NAME = "qa"
UUID = "00000000-abcabcab"
ARCH = "aarch64"
IMAGE_TYPE = "ostree"
BUILD_TARGET = "qemu"
BUILD_FORMAT = "img"
IMAGE_KEY = f"auto-osbuild-{BUILD_TARGET}-autosd9-{IMAGE_NAME}-{IMAGE_TYPE}-{ARCH}-{UUID}"
IMAGE_KEY_RIDE4_QA = f"auto-osbuild-ridesx4-autosd9-qa-regular-{ARCH}-{UUID}"
IMAGE_KEY_QDRIVE3_QA = f"auto-osbuild-qdrive3-autosd9-qa-regular-{ARCH}-{UUID}"
IMAGE_KEY_RCAR_S4_QA = f"auto-osbuild-rcar_s4-autosd9-qa-regular-{ARCH}-{UUID}"
AMI_NAME = IMAGE_KEY
COMPOSE_ID = "AutoSD-1.0-20250206.t.0"

# Mocked response data
MOCK_RESPONSE = {"ami_id": "ami-12345678", "region": "us-east-1"}


# Mocking the get_image_info function to return a controlled response
mock_image_info = {
    "UUID": UUID,
    "RELEASE": RELEASE_NAME,
    "X_STREAM": None,
    "UPSTREAM_DISTRO_ID": None,
    "UPSTREAM_DISTRO_VERSION": None,
    "COMPOSES_BASEURL": WEBSERVER_RELEASES,
    "COMPOSE_ID": COMPOSE_ID,
    "IMAGES_BASEURL": WEBSERVER_RELEASES,
    "qemu": [
        {
            "ami_name": AMI_NAME,
            "arch": ARCH,
            "build_target": "qemu",
            "image_type": IMAGE_TYPE,
            "image_name": IMAGE_NAME,
            "image_id": "ami-12345",
            "build_format": "raw",
            "debug": "false",
        },
    ],
    "aws": [
        {
            "ami_name": AMI_NAME,
            "arch": ARCH,
            "build_target": "aws",
            "image_type": IMAGE_TYPE,
            "image_name": IMAGE_NAME,
            "image_id": "ami-12345",
            "build_format": "raw",
            "debug": "false",
        },
    ],
    "qdrive3": [
        {
            "aboot_image_path": f"QDrive3/{IMAGE_KEY_QDRIVE3_QA}.aboot/aboot.img.xz",
            "arch": "aarch64",
            "build_target": "qdrive3",
            "image_type": IMAGE_TYPE,
            "image_name": "qa",
            "root_image_path": f"QDrive3/{IMAGE_KEY_QDRIVE3_QA}.aboot/rootfs.simg.xz",
            "build_format": "aboot.simg",
            "s3_upload_prefix": "QDrive3",
            "tf_pool": "qdrive3-atc",
            "debug": "false",
        },
    ],
    "ridesx4": [
        {
            "aboot_image_path": f"RideSX4/{IMAGE_KEY_RIDE4_QA}.aboot/aboot.img.xz",
            "arch": "aarch64",
            "build_target": "ridesx4",
            "image_type": IMAGE_TYPE,
            "image_name": "qa",
            "root_image_path": f"RideSX4/{IMAGE_KEY_RIDE4_QA}.aboot/rootfs.simg.xz",
            "build_format": "aboot.simg",
            "s3_upload_prefix": "RideSX4",
            "tf_pool": "ride4-atc",
            "debug": "false",
        },
    ],
    "rcar_s4": [
        {
            "image": f"{IMAGE_KEY_RCAR_S4_QA}.raw.xz",
            "arch": "aarch64",
            "build_target": "rcar_s4",
            "image_type": "ostree",
            "image_name": "qa",
            "path": f"Renesas/{IMAGE_KEY_RCAR_S4_QA}.raw.xz",
            "build_format": "raw",
            "s3_upload_prefix": "Renesas",
            "tf_pool": "rcar-s4-atc",
            "debug": "false",
        }
    ],
}


# Test validate_ami
@pytest.mark.parametrize(
    "ami_name, regions, found, exception",
    [
        # Case: AMI is found in the first region
        ("test-ami", ["us-east-1", "us-west-2"], True, None),
        # Case: AMI is not found in any region
        ("missing-ami", ["us-east-1", "us-west-2"], False, ImageInfoError),
        # Case: Error during EC2 client call
        ("test-ami", ["us-east-1"], None, ImageInfoError),
    ],
)
def test_validate_ami(ami_name, regions, found, exception):
    def mock_describe_images(Filters):
        if found is None:
            raise Exception("EC2 client error")
        return {"Images": [{"ImageId": "ami-12345"}] if found else []}

    with (
        patch("tf_requests.get_auto_compose.boto3.client") as mock_client,
        patch("os.environ.get", side_effect=lambda var: regions.pop(0) if regions else None),
    ):
        mock_ec2 = MagicMock()
        mock_ec2.describe_images.side_effect = mock_describe_images
        mock_client.return_value = mock_ec2

        if exception:
            with pytest.raises(exception):
                validate_ami(ami_name)
        else:
            validate_ami(ami_name)


# Test validate_compose


mock_tf_compose = {
    "boot_image": f"http://example.com/{IMAGE_KEY_RIDE4_QA}.aboot/aboot.img.xz",
}


@pytest.mark.parametrize(
    "tf_compose, expect_error, status",
    [
        (mock_tf_compose, False, 200),
        (mock_tf_compose, True, 404),
    ],
)
def test_validate_compose(tf_compose, expect_error, status):
    with requests_mock.Mocker() as mocker:
        mocker.head(tf_compose["boot_image"], status_code=status)
        if expect_error:
            with pytest.raises(ImageInfoError):
                validate_compose(tf_compose)
        else:
            validate_compose(tf_compose)


# Test get_image_info


def test_get_image_info():
    expected_url = f"{WEBSERVER_RELEASES}/{RELEASE_NAME}/test_images_info.json"

    with requests_mock.Mocker() as mocker:
        mocker.get(expected_url, json=MOCK_RESPONSE)

        result = get_image_info(WEBSERVER_RELEASES, RELEASE_NAME)
        assert result == MOCK_RESPONSE


def test_get_image_info_failure():
    failing_url = f"{WEBSERVER_RELEASES}/{RELEASE_NAME}/test_images_info.json"

    with requests_mock.Mocker() as mocker:
        mocker.get(failing_url, status_code=404)

    with pytest.raises(ImageInfoError):
        get_image_info(WEBSERVER_RELEASES, RELEASE_NAME)


# Test calculate_auto_compose


@pytest.mark.parametrize(
    "config, expected",
    [
        # config passed and has all the required items
        (
            {
                "webserver_releases": "http://example.com/releases",
                "RELEASE_NAME": RELEASE_NAME,
                "DEBUG_KERNEL": "false",
                "IMAGE_NAME": IMAGE_NAME,
                "IMAGE_TYPE": IMAGE_TYPE,
                "ARCH": ARCH,
                "HW_TARGET": "aws",
                "x_stream": None,
            },
            AMI_NAME,
        ),
        # config passed but is missing some items
        (
            {
                "webserver_releases": "http://example.com/releases",
                "x_stream": None,
                "IMAGE_TYPE": IMAGE_TYPE,
            },
            AMI_NAME,
        ),
    ],
)
def test_calculate_auto_compose_config(config, expected):
    with (
        patch("tf_requests.get_auto_compose.get_image_info", return_value=mock_image_info),
        patch("tf_requests.get_auto_compose.validate_ami", return_value=None),
    ):
        _, compose = calculate_auto_compose(config)
        assert compose == expected


@pytest.mark.parametrize(
    "env_vars, config, expected",
    [
        # config not passed but all required items are in os.environ
        (
            {
                "webserver_releases": "http://example.com/releases",
                "RELEASE_NAME": RELEASE_NAME,
                "DEBUG_KERNEL": "false",
                "IMAGE_NAME": IMAGE_NAME,
                "IMAGE_TYPE": IMAGE_TYPE,
                "ARCH": ARCH,
                "HW_TARGET": "aws",
                "x_stream": None,
            },
            None,
            AMI_NAME,
        ),
        # config not passed but some required items are missing in os.environ
        (
            {
                "webserver_releases": "http://example.com/releases",
                "x_stream": None,
                "IMAGE_TYPE": IMAGE_TYPE,
            },
            None,
            AMI_NAME,
        ),
    ],
)
def test_calculate_auto_compose_env_vars(env_vars, config, expected):
    with (
        patch("os.environ", new=env_vars),
        patch("tf_requests.get_auto_compose.get_image_info", return_value=mock_image_info),
        patch("tf_requests.get_auto_compose.validate_ami", return_value=None),
    ):
        _, compose = calculate_auto_compose(config)
        assert compose == expected


# Prepare mocks for the calculate_auto_compose function
# That function converts the IMAGE_NAME to 'qa' unless it's explicitly set to other
AMI_NAME_QA = f"auto-osbuild-qemu-autosd9-qa-ostree-{ARCH}-{UUID}"

mock_qdrive3_compose = {
    "boot_image": f"http://example.com/releases/ER0/QDrive3/{IMAGE_KEY_QDRIVE3_QA}.aboot/aboot.img.xz",
    "boot_checksum": f"http://example.com/releases/ER0/QDrive3/{IMAGE_KEY_QDRIVE3_QA}.aboot/aboot.img.xz.sha256",
    "root_image": f"http://example.com/releases/ER0/QDrive3/{IMAGE_KEY_QDRIVE3_QA}.aboot/rootfs.simg.xz",
    "root_checksum": f"http://example.com/releases/ER0/QDrive3/{IMAGE_KEY_QDRIVE3_QA}.aboot/rootfs.simg.xz.sha256",
}
mock_ridesx4_compose = {
    "boot_image": f"http://example.com/releases/ER0/RideSX4/{IMAGE_KEY_RIDE4_QA}.aboot/aboot.img.xz",
    "boot_checksum": f"http://example.com/releases/ER0/RideSX4/{IMAGE_KEY_RIDE4_QA}.aboot/aboot.img.xz.sha256",
    "root_image": f"http://example.com/releases/ER0/RideSX4/{IMAGE_KEY_RIDE4_QA}.aboot/rootfs.simg.xz",
    "root_checksum": f"http://example.com/releases/ER0/RideSX4/{IMAGE_KEY_RIDE4_QA}.aboot/rootfs.simg.xz.sha256",
}


@pytest.mark.parametrize(
    "hw_target, expected",
    [
        # hw_target == aws
        ("aws", AMI_NAME_QA),
        # hw_target != aws AND in ['qdrive3, 'ridesx4']
        ("qdrive3", json.dumps(mock_qdrive3_compose)),
        ("ridesx4", json.dumps(mock_ridesx4_compose)),
    ],
)
def test_calculate_auto_compose_hw_target(hw_target, expected):
    config = {
        "webserver_releases": "http://example.com/releases",
        "RELEASE_NAME": RELEASE_NAME,
        "DEBUG_KERNEL": "false",
        "ARCH": ARCH,
        "IMAGE_TYPE": IMAGE_TYPE,
        "HW_TARGET": hw_target,
        "x_stream": None,
    }
    with (
        patch("tf_requests.get_auto_compose.get_image_info", return_value=mock_image_info),
        patch("tf_requests.get_auto_compose.validate_compose", return_value=None),
        patch("tf_requests.get_auto_compose.validate_ami", return_value=None),
    ):
        _, compose = calculate_auto_compose(config)
        assert compose == expected


def test_calculate_auto_compose_hw_target_not_supported():
    config = {
        "webserver_releases": "http://example.com/releases",
        "RELEASE_NAME": RELEASE_NAME,
        "DEBUG_KERNEL": "false",
        "ARCH": ARCH,
        "HW_TARGET": "not_supported",
        "x_stream": None,
    }
    with patch(
        "tf_requests.get_auto_compose.get_image_info",
        side_effect=ImageInfoError(),
    ):
        result = calculate_auto_compose(config)
        assert result is None


def test_get_auto_compose_id():
    with patch("tf_requests.get_auto_compose.get_image_info", return_value=mock_image_info):
        assert get_auto_compose_id(WEBSERVER_RELEASES, RELEASE_NAME) == COMPOSE_ID
