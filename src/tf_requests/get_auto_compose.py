#!/usr/bin/env python3

"""
get_auto_compose command will fetch information about an specific automotive image available
for doing automated test in Testing Farm and it'll return the string with a valid Testing Farm
compose for it.

it fetchs the information from a file like this one:
https://autosd.sig.centos.org/AutoSD-9/nightly/test_images_info.json

It can fect information about the last image built (nighlt) or an specific formal release
(like 'ER3' or similar).

By default, it'll return the compose for the QA image, but it can also show the compose for other
images like 'cki', 'ps' or 'developer'.

It also supports a board based images, that's it images to be flashed on boards like QDrive3 or
RideSX4 using the android boot system (aboot).

This script can be used standalone or its functions can be called from another script.
"""
import json
import logging
import os
import re
import sys
from http.client import HTTPConnection
from typing import Any, Dict, Optional, Tuple

import boto3
import requests
from botocore.exceptions import BotoCoreError, ClientError
from requests.exceptions import RequestException

from .request_utils import HEADER_NO_CACHE

LOGLEVEL = os.environ.get("LOGLEVEL", "INFO").upper()
logging.basicConfig(level=LOGLEVEL, format="%(asctime)s - %(levelname)s - %(message)s")
if LOGLEVEL == "DEBUG":
    HTTPConnection.debuglevel = 1

VALID_IMAGES_FOR_TESTING = ["qa", "qm", "cki"]
IMAGES_INFO_FILE = "test_images_info.json"
ABOOT_TARGETS = ["qdrive3", "ridesx4"]
AMI_HW_TARGETS = ["qemu", "aws"]


class ImageInfoError(Exception):
    pass


def validate_ami(ami_name: str) -> None:
    """
    Validate ami_name whether the it exists in EC2

    Args:
        ami_name (str): The AMI name for checking in EC2.
    Raises:
        ImageInfoError: If there is an issue retriving AMIs from the EC2.
    """
    try:
        aws_regions = [os.environ.get("AWS_TF_REGION"), os.environ.get("AWS_CKI_REGION")]
        ami_found = False
        for region in filter(None, aws_regions):
            logging.info(f"Searching for AMI in region: {region}")
            ec2 = boto3.client("ec2", region_name=region)
            response = ec2.describe_images(Filters=[{"Name": "name", "Values": [ami_name]}])
            if response["Images"]:
                ami_found = True
                break
        if not ami_found:
            raise ImageInfoError(f"AMI {ami_name} not found in any of the regions: {aws_regions}")
    except (Exception, BotoCoreError, ClientError) as e:
        raise ImageInfoError(f"Validation failed: {e}")


def validate_compose(tf_compose: Dict) -> None:
    """
    Validate tf_compose whether the generated URLs are reachable and exists

    Args:
        tf_compose (Dict): The Testing Farm compose for the requested image.
        ami_name (str): The AMI name for checking in EC2. Default is None
    Raises:
        ImageInfoError: If there is an issue retriving composes from the urls in compose.
    """
    try:
        for key, url in tf_compose.items():
            response = requests.head(url, allow_redirects=True)
            if response.status_code != 200:
                raise ImageInfoError(f"URL for tf_compose[{key}] is not reachable: {url}")
    except RequestException as e:
        raise ImageInfoError(f"Error validating URL for tf_compose[{key}]: {url} - {e}")


def get_image_info(webserver_releases: str, release_name: str) -> Dict[str, Any]:
    """
    Fetch image metadata from a JSON file hosted at a webserver release URL.

    Args:
        webserver_releases (str): Base URL where the images information file is stored.
        release_name (str): The name of the image's release (e.g., 'nightly' or 'ER3').

    Returns:
        Dict[str, Any]: Dictionary containing metadata for the published AMI.

    Raises:
        ImageInfoError: If there is an issue retrieving the data from the server.
    """
    images_info_url = f"{webserver_releases}/{release_name}/{IMAGES_INFO_FILE}"
    try:
        response = requests.get(images_info_url, headers=HEADER_NO_CACHE)
        response.raise_for_status()
    except RequestException as e:
        raise ImageInfoError(f"Error downloading the file from {images_info_url}. Error: {e}")

    return response.json()


def calculate_auto_compose(config: Optional[Dict[str, str]] = None) -> Optional[Tuple[str, str]]:
    """
    Calculate and return a string with a valid Testing Farm compose.

    It takes information from a 'config' dictionary passed as args or (if not provided), it takes
    the information from the environment.

    It'll update some items (IMAGE_NAME, IMAGE_TYPE, IMAGE_KEY and UUID) for the 'config' dictionary if
    it's passed as argument, but just if the HW_TARGET is not 'aws'.

    Args:
        config Optional[Dict[str, str]]: Dictionary with the program configuration. It should have
            some items (webserver_releases, RELEASE_NAME, DEBUG_KERNEL, IMAGE_NAME, ARCH, HW_TARGET).
            If not, it'll use the default values.

    Returns:
        Optional[Tuple[str, str]]: A tuple containing:
            - tf_pool: The Testing Farm pool for that type of boards
            - tf_compose: The Testing Farm compose for the requested image.
            It'll return None if there was a problem for getting the image information.
    """
    config = config or os.environ.copy()

    # Update these the config with these default values if they are not set
    config.setdefault("RELEASE_NAME", "nightly")
    config.setdefault("IMAGE_NAME", "qa")

    # Obtain some values to work with
    webserver_releases = config["webserver_releases"]
    if not webserver_releases:
        logging.error("webserver_releases must be provided in the config.")
        return None

    release_name = config["RELEASE_NAME"]
    image_name = config["IMAGE_NAME"]
    arch = config.get("ARCH", "aarch64")
    hw_target = config.get("HW_TARGET", "aws" if image_name == "cki" else "qemu")
    image_type = config.get("IMAGE_TYPE", "ostree" if hw_target in AMI_HW_TARGETS else "regular")
    tf_pool = config.get("tf_pool", "")
    image_info = None

    try:
        images = get_image_info(webserver_releases, release_name)
        # Find the matching image in the data
        image_info = next(
            img
            for img in images[hw_target]
            if img["image_name"] == image_name and img["arch"] == arch and img["image_type"] == image_type
        )
        if not image_info:
            raise ImageInfoError(f"Error: the image_name '{image_name}' could not be found.")

        # Update configuration with new image info
        config["UUID"] = images["UUID"]
        config["IMAGE_TYPE"] = image_info["image_type"]

        if hw_target in AMI_HW_TARGETS:
            config["IMAGE_KEY"] = image_info["ami_name"]
            validate_ami(image_info["ami_name"])
            return tf_pool, config["IMAGE_KEY"]

        if hw_target in ABOOT_TARGETS:
            match = re.search("[^/]*osbuild[^/]+", image_info["aboot_image_path"])
            if match:
                config["IMAGE_KEY"] = match.group(0)
        else:
            config["IMAGE_KEY"] = image_info["image"].rstrip(".raw.xz")

        tf_pool = image_info["tf_pool"]

        # Create structure for tf_compose
        tf_compose = (
            {
                "boot_image": f"{webserver_releases}/{release_name}/{image_info['aboot_image_path']}",
                "boot_checksum": f"{webserver_releases}/{release_name}/{image_info['aboot_image_path']}.sha256",
                "root_image": f"{webserver_releases}/{release_name}/{image_info['root_image_path']}",
                "root_checksum": f"{webserver_releases}/{release_name}/{image_info['root_image_path']}.sha256",
            }
            if hw_target in ABOOT_TARGETS
            else {
                "disk_image": f"{webserver_releases}/{release_name}/{image_info['path']}",
                "disk_checksum": f"{webserver_releases}/{release_name}/{image_info['path']}.sha256",
            }
        )
        validate_compose(tf_compose)
        return tf_pool, json.dumps(tf_compose)
    except StopIteration:
        logging.error(
            f"No image found in test_images_info.json matching hw_target: '{hw_target}', "
            f"image_name: '{image_name}', image_type: '{image_type}', and arch: '{arch}'."
        )
        return None
    except ImageInfoError as e:
        logging.error(e)
        return None


def get_auto_compose_id(webserver_releases: str, release_name: str) -> str:
    """
    Fetch compose ID for the requested image.

    Args:
        webserver_releases (str): Base URL where the images information file is stored.
        release_name (str): The name of the image's release (e.g., 'nightly' or 'ER3').

    Returns:
        str: The value of the compose id.
    """
    return get_image_info(webserver_releases, release_name)["COMPOSE_ID"]


def main() -> None:
    result = calculate_auto_compose()
    if not result:
        # Exit with error, but different from 1, in case the job allow failure with 1
        sys.exit(2)
    _, compose = result
    print(compose)


if __name__ == "__main__":
    main()
